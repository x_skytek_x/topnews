const path = require('path');
const fs = require('fs');

const { GenerateSW } = require("workbox-webpack-plugin");

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  configureWebpack: {
    plugins: [
      new GenerateSW({
        clientsClaim: true,
      })
    ]
  },

  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [
        path.resolve(__dirname, "./src/assets/scss/main.sass")
      ]
    }
  },

  pwa: {
    themeColor: '#10163A',
    msTileColor: '#10163A'
  }
}
