/**
 * @author Milos Zrilic
 * @description Merge default and custom configs. Use this file when calling configs.
 */

var defaultConfig = require("./config.default");

/**
 * Merge two config in same object if property on new object is set
 * @param {Object} defaultConfig 
 * @param {Object} newConfig 
 */
function merge(defaultConfig, newConfig) {
    for (let defaultConfigProperty in defaultConfig) {
        if (newConfig[defaultConfigProperty]) {
            if (typeof newConfig[defaultConfigProperty] === 'object' 
                && newConfig[defaultConfigProperty] != null 
                && newConfig[defaultConfigProperty] instanceof Array) {

                for (let newConfigProperty in newConfig[defaultConfigProperty]) {
                    merge(newConfig[defaultConfigProperty], newConfig[defaultConfigProperty][newConfigProperty]);
                }
            } else {
                defaultConfig[defaultConfigProperty] = newConfig[defaultConfigProperty];
            }
        }
    }

    return defaultConfig;
}


var customConfig = {};
try {
    customConfig = require('./config.custom');
} catch (error) {
    console.log(`Using confi from config.default!`);
} finally {
    defaultConfig = merge(defaultConfig, customConfig);
}

module.exports = defaultConfig;