/**
 * @description Default configurations. They can be overwritten by config.custom.js
 */
module.exports = {
    apiKey: "1c4fccf1801b45789570add386c92eda",
    axios: {
        protocol: 'https',
        url: 'newsapi.org',
        version: 'v2',
        options: {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        }
    },
    fallbackCountry: {
        code: 'us',
        name: 'United States of America'
    }
}