import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/top-news'
  },
  {
    path: "/top-news",
    name: "topnews",
    component: () => import(/* webpackChunkName: "topnews-view" */ "../views/TopNewsView.vue"),
  },
  {
    path: "/top-news/:category",
    name: "topnews", // same name to mark to news section
    component: () => import(/* webpackChunkName: "topnews-view" */ "../views/CategoryView.vue"),
    props: true
  },
  {
    path: "/single-news",
    name: "singlenews", // same name to mark to news section
    component: () => import(/* webpackChunkName: "singlenews-view" */ "../views/SingleNewsView.vue"),
    props: true
  },
  {
    path: "/categories",
    name: "categories",
    component: () => import(/* webpackChunkName: "categories-view" */ "../views/CategoriesView.vue")
  },
  {
    path: "/search",
    name: "search",
    component: () => import(/* webpackChunkName: "search-view" */ "../views/SearchView.vue")
  },
  {
    path: "*",
    redirect: "/top-news"
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  await store.restored;
  next();
})

export default router
