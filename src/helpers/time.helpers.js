import moment from 'moment'

class TimeHelpers {

    /**
     * @description Format date and time by provided format or use default one
     * @param {String} dateTimestring 
     * @param {String} format 
     */
    static formatDateAndTime(dateTimestring, format = 'DD/MM/YYYY hh:mm:ss') {
        return moment(dateTimestring).utcOffset(dateTimestring).format(format);
    }
}

export default TimeHelpers;