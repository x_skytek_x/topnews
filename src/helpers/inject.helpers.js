/**
 * @description Service plguin holds data to all helpers
 */
import TimeHelpers from './time.helpers'
import GlobalHelpers from './global.helpers'
/**
 * Abstract class InjectHelpers
 */
export function InjectHelpers () {

}

/**
 * @description install function gets called by Vue.use()
 */
InjectHelpers.install  = function (Vue) {

    // Provide httpService in instance
    Vue.prototype.$timeHelpers = TimeHelpers;
    Vue.prototype.$globalHelpers = GlobalHelpers;
}