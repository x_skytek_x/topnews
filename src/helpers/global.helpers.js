let FallbackImage = require('../assets/images/no-image-provided.png');

class GlobalHelpers {

    static getImageUrl(image, fallbackImage = null) {
        return image ? image : fallbackImage ? fallbackImage : FallbackImage;
    }
}

export default GlobalHelpers;