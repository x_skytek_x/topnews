import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import './registerServiceWorker'

import wb from "./registerServiceWorker";

import { InjectService } from './services/inject.service'
import { InjectHelpers } from './helpers/inject.helpers'
import { InjectDirectives } from './directives/inject.directives'

import vuetify from './plugins/vuetify';

import '@/assets/scss/main.sass'

Vue.config.productionTip = false

Vue.use(InjectService);
Vue.use(InjectHelpers);
Vue.use(InjectDirectives);

Vue.prototype.$workbox = wb;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
