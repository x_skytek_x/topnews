import * as mutationTypes from './mutation-types';

/**
 * @description Set county status to enabled
 * @param {Object} param0 
 * @param {Boolean} enabled 
 */
export const setCountriesEnabled = function ({commit}, enabled) {
    commit(mutationTypes.SET_COUNTRY_ENABLED, {
        enabled: enabled ? enabled : false
    });
}

/**
 * 
 * @param {Object} param0 
 * @param {String} code 
 */
export const setActiveCountry = function ({commit}, code) {
    commit(mutationTypes.SET_ACTIVE_COUNTRY, {
        code: code ? code : ""
    });
}

/**
 * 
 * @param {Object} param0 
 * @param {String} identifier 
 */
export const setActiveHeaderItem = function ({commit}, identifier) {
    commit(mutationTypes.SET_ACTIVE_HEADER_ITEM, {
        identifier: identifier ? identifier : ""
    });
}

/**
 * @description Set navigation drawer status
 * @param {Object} param0 
 * @param {Boolean} opened 
 */
export const setNavigationDrawerOpened = function ({commit}, opened) {
    commit(mutationTypes.SET_NAVIGATION_DRAWER_OPENED, {
        opened: opened ? opened : false
    });
}

/**
 * @description Set navigation drawer status
 * @param {Object} param0 
 * @param {Object} param1
 */
export const setCategoryUrl = function ({commit}, { category, url }) {
    commit(mutationTypes.SET_CATEGORY_URL, {
        category: category ? category : "",
        url: url ? url : ""
    });
}

/**
 * @description Set navigation drawer status
 * @param {Object} param0 
 * @param {Object} param1
 */
export const setCategoryTopHeadlines = function ({commit}, { category, topHeadlines }) {
    commit(mutationTypes.SET_CATEGORY_TOPHEADLINES, {
        category: category ? category : "",
        topHeadlines: topHeadlines ? topHeadlines : []
    });
}

/**
 * @description Reset state in store per key
 * @param {Object} param0 
 * @param {String} key
 */
export const setStateToDefaultPerKey = function ({commit}, key) {
    commit(mutationTypes.SET_STATE_TO_DEFAULT_PER_KEY, {
        key: key
    });
}

/**
 * @description Set total number of results
 * @param {Object} param0 
 * @param {Object} param1 
 */
export const setCategoryTotalResults = function ({commit}, { category, totalResults }) {
    commit(mutationTypes.SET_CATEGORY_TOTAL_RESULTS, {
        category: category ? category : "",
        totalResults: totalResults ? totalResults : 0
    });
}

/**
 * @description Set query page of category
 * @param {Object} param0 
 * @param {Object} param1 
 */
export const setCategoryPage = function ({commit}, { category, page }) {
    commit(mutationTypes.SET_CATEGORY_PAGE, {
        category: category ? category : "",
        page: page ? page : 0
    });
}

/**
 * @description Set news so user can move back to them and see it if page gets refreshed
 * @param {Object} param0 
 * @param {Object} news 
 */
export const setSingleNews = function ({commit}, news) {
    commit(mutationTypes.SET_SINGLE_NEWS, news);
}

/**
 * @description Save search param to use it after country change
 * @param {Object} param0 
 * @param {String} news 
 */
export const setCurrentSearch = function ({commit}, search) {
    commit(mutationTypes.SET_CURRENT_SEARCH, search);
}
