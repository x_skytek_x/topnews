/**
 * @description Map these getters as computed properties to always get newest values
 */
export default {
    getHeaderSectionLeftItems: (state) => {
        return state.headerSectionLeftItems;
    },
    getHeaderSectionRightItems: (state) => {
        return state.headerSectionRightItems;
    },
    getCountries: (state) => {
        return state.countries;
    },
    isNavigationDrawerOpened: (state) => {
        return state.navigationDrawerOpened;
    },
    getActiveCountry: (state) => {
        for (let country of state.countries) {
            if (country.active) {
                return country;
            }
        }
    },
    getNewsCategories: (state) => {
        return state.categories;
    },
    getNewsCategory: (state) => (category) => {
        return state.categories[category];
    },
    getTotalResultsInCategory: (state) => (category) => {
        return state.categories[category].totalResults;
    },
    getCategoryPage: (state) => (category) => {
        return state.categories[category].query.page;
    },
    getCategoryUrl: (state) => (category) => {
        return state.categories[category].query.url;
    },
    getSingleNews: (state) => {
        return state.singleNews;
    },
    getCurrentSearch: (state) => {
        return state.currentSearch;
    }
}