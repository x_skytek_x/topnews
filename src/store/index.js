import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)
import mutations from './mutations'
import * as actions from './actions'
import getters from './getters'

export function getInitialState() {
  return {
    // Items in left header section
    headerSectionLeftItems: [
      {
        identifier: 'topnews',
        name: 'Top news',
        active: true,
        to: '/top-news',
        icon: 'far fa-newspaper'
      },
      {
        identifier: 'categories',
        name: 'Categories',
        active: false,
        to: '/categories',
        icon: 'fas fa-th-large'
      },
      {
        identifier: 'search',
        name: 'Search',
        active: false,
        to: '/search',
        icon: 'fas fa-search'
      }
    ],
    // Items in left header section
    headerSectionRightItems: [],
    countries: [
      {
        code: 'us',
        name: 'United States of America',
        enabled: true,
        active: true
      },
      {
        code: 'gb',
        name: 'Great Britain',
        enabled: true,
        active: false
      }
    ],
    // Status of navigation drawer
    navigationDrawerOpened: false,
    singleNews: {},
    currentSearch: "",
    categories: {
      'general': {
        category: 'general',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      },
      'entertainment': {
        category: 'entertainment',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      },
      'health': {
        category: 'health',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      },
      'science': {
        category: 'science',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      },    
      'sport': {
        category: 'sport',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      },
      'technology': {
        category: 'technology',
        query: {
          url: "",
          pageSize: 5,
          page: 0
        },
        totalResults: 0,
        topHeadlines: [],
        expand: false
      }
    }
  };
}

export default new Vuex.Store({
  state: getInitialState(),
  mutations,
  actions,
  getters,
  plugins: [new VuexPersistence({
    key: "topnews"
  }).plugin]
})
