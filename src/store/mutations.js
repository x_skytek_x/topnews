import * as mutationTypes from './mutation-types';
import { getInitialState } from './index';

export default {
    /**
     * @description Only one country can be selected
     * @param {String} code of country
     */
    [mutationTypes.SET_ACTIVE_COUNTRY]: (state, {code}) => {
        for (let existingCountry of state.countries) {
            if (existingCountry.code) {
                // Double == used to match both 'COUNTRY' and 'country'
                existingCountry.active = existingCountry.code == code ? true : false;
            }
        }
    },
    /**
     * @description Enables or disables available countries
     * @param {Boolean} enabled
     */
    [mutationTypes.SET_COUNTRY_ENABLED]: (state, {enabled}) => {
        for (let country of state.countries) {
            country.enabled = enabled

            console.log(`Country: ${country.name} is enabled: ${country.enabled}`)
        }
    },
    /**
     * @description Mark specific header item as active
     * @param {String} identifier
     */
    [mutationTypes.SET_ACTIVE_HEADER_ITEM]: (state, {identifier}) => {
        for (let item of state.headerSectionLeftItems) {
            item.active = item.identifier === identifier ? true : false;
        }
        for (let item of state.headerSectionRightItems) {
            item.active = item.identifier === identifier ? true : false;
        }
    },
    /**
     * @description Mutation for changing state of sidebar drawer
     * @param {Boolean} opened
     */
    [mutationTypes.SET_NAVIGATION_DRAWER_OPENED]: (state, {opened}) => {
        state.navigationDrawerOpened = opened;
    },
    /**
     * @description Change url of specific category
     * @param {Object} param0
     */
    [mutationTypes.SET_CATEGORY_URL]: (state, {category, url}) => {
        for (let cat in state.categories) {
            if (state.categories[cat].category === category) {
                state.categories[cat].query.url = url;
            }
        }
    },
    /**
     * @description Set topheadlines for specific category
     * @param {Object}param0
     */
    [mutationTypes.SET_CATEGORY_TOPHEADLINES]: (state, {category, topHeadlines}) => {
        for (let cat in state.categories) {
            if (state.categories[cat].category === category) {
                // state.categories[cat].topHeadlines = [...state.categories[cat].topHeadlines,...topHeadlines]
                state.categories[cat].topHeadlines = topHeadlines;
            }
        }
    },
    /**
     * @description Set default state for specified key
     */
    [mutationTypes.SET_STATE_TO_DEFAULT_PER_KEY]: (state , {key}) => {
        state[key] = getInitialState()[key];
    },
    /**
     * @description Set total number of results in specific category
     */
    [mutationTypes.SET_CATEGORY_TOTAL_RESULTS]: (state, {category, totalResults}) => {
        for (let cat in state.categories) {
            if (state.categories[cat].category === category) {
                state.categories[cat].totalResults = totalResults;
            }
        }
    },
    /**
     * @description Set query page in specific category
     */
    [mutationTypes.SET_CATEGORY_PAGE]: (state, {category, page}) => {
        for (let cat in state.categories) {
            if (state.categories[cat].category === category) {
                state.categories[cat].query.page = page;
            }
        }
    },
    /**
     * @description Store current single news in case someone refreshes route
     */
    [mutationTypes.SET_SINGLE_NEWS]: (state, news) => {
        state.singleNews = news;
    },
    /**
     * @description Store search for usage on country change
     */
    [mutationTypes.SET_CURRENT_SEARCH]: (state, search) => {
        state.currentSearch = search;
    }
}