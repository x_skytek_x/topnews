/**
 * @description Cut text at specific character
 */

const _exceptr = (el, arg, value) => {
    let append = false;
    if (el.innerHTML.length > parseInt(arg)) {
        append = true;
    }
    let exceptrArg = arg ? arg.split(":")[0] ? arg.split(":")[0] : arg : 10;
    let val = value ? value : "...";
    if (append) {
        el.innerHTML = el.innerHTML.slice(0, exceptrArg) + val;
    }
}
module.exports.exceptr = {
    bind: function (el, {arg, value}) {
        _exceptr(el, arg, value);
    },
    updated: function (el, {arg, value}) {
        _exceptr(el, arg, value);
    },
    inserted: function (el, {arg, value}) {
        _exceptr(el, arg, value);
    }, 
    componentUpdated: function (el, {arg, value}) {
        _exceptr(el, arg, value);
    }
}