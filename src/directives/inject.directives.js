/**
 * @description Directives plguin holds data for all directives
 */
import { exceptr } from './exceptr.directive'
/**
 * Abstract class InjectDirectives
 */
export function InjectDirectives () {

}

/**
 * @description install function gets called by Vue.use()
 */
InjectDirectives.install  = function (Vue) {

    // Provide httpService in instance
    Vue.directive('exceptr', exceptr)
}