/*eslint no-prototype-builtins: "error"*/
import axios from 'axios';
import config from '../config/index'

/**
 * @description HttpService provides wrapper around axios library in case dependencies are getting changed, 
 * we can easly handle it at one place
 */
export default new class HttpService {
    constructor(options = {}) {
        this._options = {...config.axios.options, ...options};
    }

    /**
     * @description Send GET request to provided endpoint
     * @param {String} url 
     * @param {Object} options 
     */
    async get(url, queryParams = {}, options = {}) {
        var output = {
            data: null,
            message: "",
            status: 'error',
            totalResults: 0,
            queryParams
        };
        try {

            let response = await axios.get(`${config.axios.protocol}://${config.axios.url}/${config.axios.version}${url}&apiKey=${config.apiKey}`, options);
            console.log(`urL : ${config.axios.protocol}://${config.axios.url}/${config.axios.version}${url}&apiKey=${config.apiKey}`)
            let result = response.data && response.data !== null && response.data.articles ? response.data.articles : null;
            let status = response.data && response.data !== null && response.data.status ? response.data.status : 'error';
            let totalResults = response.data && response.data !== null && response.data.totalResults ? response.data.totalResults : 0;
            output = {...output, ...{
                data: result,
                status: status,
                totalResults: totalResults
            }};
        } catch (error) {
            console.error(`ERR: ${error}`)
            // Output should contain cought errors
            output = {
                ...output, 
                data: null,
                message: error.response.data.message
            }
            
        }
        return output;
    }

    /**
     * @description Send POST request to provided endpoint
     * @param {String} url 
     * @param {Object} data 
     * @param {Object} options 
     */
    async post(url, data = {}, options) {
        var output = {
            data: null,
            message: "",
            status: 'error',
            totalResults: 0
        };
        try {
            let response = await axios.post(`${config.axios.protocol}://${config.axios.url}/${config.axios.version}${url}&apiKey=${config.apiKey}`, data, options);
            let result = response.data && response.data !== null && response.data.articles ? response.data.articles : null;
            let status = response.data && response.data !== null && response.data.status ? response.data.status : 'error';
            let totalResults = response.data && response.data !== null && response.data.totalResults ? response.data.totalResults : 0;
            output = {...output, ...{
                data: result,
                status: status,
                totalResults: totalResults
            }};
        } catch (error) {
            // Output should contain cought errors
            output = {
                ...output,
                message: error.message
            }
            
        }
        return output;
    }
}