/**
 * @description Service plguin holds data to all singleton services, config and other app relevant object which are used all over application
 */
import config from '../config/index'
import HttpService from './http.service'
/**
 * Abstract class InjectService
 */
export function InjectService () {

}

/**
 * @description install function gets called by Vue.use()
 */
InjectService.install  = function (Vue) {

    // Define config in instance
    Vue.prototype.$configuration = config;
    // Provide httpService in instance
    Vue.prototype.$httpService = HttpService;
}